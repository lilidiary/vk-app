import React from 'react';
import Async from 'react-async';
import {
  PanelHeader,
  Snackbar,
  Button,
  Group,
  List,
  Cell,
  Counter,
  Link,
  PullToRefresh
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import axios from 'axios';
import Icon24User from '@vkontakte/icons/dist/24/user';
import Icon24Users from '@vkontakte/icons/dist/24/users';
import moment from 'moment';
import 'moment/locale/ru';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import { getUser } from '../../modules/getUser';

moment.locale('ru');
let BOT_API = 'https://api.lildiary.ru';
const currentURL = new URL(document.location);
if (currentURL.host === 'localhost') {
  BOT_API = 'http://localhost:81';
}
class Admin extends React.Component {
  constructor(props) {
    super();
    this.state = {
      snackbar: null,
      data: { configs: [], lastEvents: [] },
      fetching: false
    };
    this.vkData = props.vkData;
    this.callback = props.callback;
  }

  componentDidMount() {
    this.updateData();
  }

  updateData() {
    this.setState({ fetching: true });
    axios
      .post(`${BOT_API}/api/info`, { vkData: this.vkData })
      .then(({ data }) => {
        this.callback('state', { popout: null });
        this.setState({ data, fetching: false });
      }).catch(() => {
        this.setState({ fetching: false });
        this.callback('state', { popout: null });
        this.openSnackbar('Неизвестная ошибка');
      });
  }

  shutdown() {
    axios
      .post(`${BOT_API}/api/shutdown`, { vkData: this.vkData });
  }

  openSnackbar(text) {
    this.setState({
      snackbar:
        <Snackbar
          onClose={() => this.setState({ snackbar: null })}
        > {text}
        </Snackbar>
    });
  }

  render() {
    const {
      usersAll,
      usersLogged,
      usersDone,
      usersNotLogged,
      configs,
      lastEvents } = this.state.data;
    const { snackbar } = this.state;
    const services = configs.map((item) =>
      (
        <Group title={item.title || item.service}>
          <Cell
            description={(item.ping || item.lastStart) ? moment(item.ping || item.lastStart).fromNow() : 'Никогда'}
            bottomContent={<Button>Просмотреть</Button>}
            size="l"
          >
            {item.description || 'Нет описания'}
          </Cell>
        </Group>
      ));

    const events = lastEvents.map((item) => {
      let link = null;
      if (item.user) {
        link = (
          <Async promiseFn={() => getUser(BOT_API, item.user, this.vkData)}>
            {({ data, error, isPending }) => {
              if (error || isPending) {
                return null;
              }
              if (data) {
                ({ data } = data);
                if (!data.success) {
                  return null;
                }
                return <Link href={`https://vk.com/id${data.user.vkID}`}>{data.user.fname} {data.user.sname}</Link>;
              }
              return null;
            }}
          </Async>
        );
      }
      return (
        <Cell description={item.time ? moment(item.time).fromNow() : 'неизвестно'}>
          {link} {item.text}
        </Cell>
      );
    });
    return (
      <div>
        <PanelHeader><Icon24Back onClick={() => this.callback('panel', 'login')} />lilDiary | Administration</PanelHeader>
        <PullToRefresh onRefresh={() => this.updateData()} isFetching={this.state.fetching}>
          <Group title="Информация по пользователям">
            <List>
              <Cell before={<Icon24User />} indicator={<Counter>{usersAll}</Counter>}>Всего пользователей</Cell>
              <Cell before={<Icon24Users />} indicator={<Counter>{usersLogged}</Counter>}>Пользователей авторизовано</Cell>
              <Cell before={<Icon24User />} indicator={<Counter>{usersDone}</Counter>}>Пользователей могут использовать</Cell>
              <Cell before={<Icon24User />} indicator={<Counter>{usersNotLogged}</Counter>}>Пользователей не авторизовано</Cell>
            </List>
          </Group>
          {services}
          <Group title="Последние действия">
            <List>
              {events}
            </List>
          </Group>
          <Group title="Убить процесс бота">
            <Button level="destructive" size="xl" onClick={() => this.shutdown()}>Убить</Button>
          </Group>
          {snackbar}
        </PullToRefresh>
      </div>
    );
  }
}

export default Admin;
