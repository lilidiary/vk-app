import React from 'react';
import {
  Placeholder,
  PanelHeader,
  Snackbar,
  Button
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import axios from 'axios';
import Icon56ErrorOutline from '@vkontakte/icons/dist/56/error_outline';

class Main extends React.Component {
  constructor(props) {
    super();
    this.state = { criticalError: false, chooseRegion: false, exists: true };
    this.SERVER_API = props.SERVER_API;
    this.vkData = props.vkData;
    this.callback = props.callback;
    axios
      .post(`${this.SERVER_API}/api/info`, { vkData: this.vkData })
      .then(({ data }) => {
        this.callback('state', { popout: null });
        if (data.admin) {
          this.callback('panel', 'admin');
          return;
        }

        if (data.logged) {
          this.callback('panel', 'logged');
          return;
        }
        if (!data.exists) {
          this.setState({ exists: false });
          return;
        }
        if (data.url) {
          this.callback('panel', data.goToGosuslugi ? 'gosuslugi' : 'login');
          return;
        }
        this.setState({ chooseRegion: true });
        this.openSnackbar('Вы не выбрали регион');
      }).catch(() => {
        this.callback('state', { popout: null });
        this.setState({ criticalError: true });
        this.openSnackbar('Неизвестная ошибка. Мы уже её исправляем');
      });
  }

  openSnackbar(text) {
    this.setState({
      snackbar:
        <Snackbar
          onClose={() => this.setState({ snackbar: null })}
        > {text}
        </Snackbar>
    });
  }

  render() {
    const { snackbar, criticalError, chooseRegion, exists } = this.state;
    const error = (
      <div>
        <Placeholder
          icon={<Icon56ErrorOutline />}
          title="Мы сожалеем, но.."
          action={<Button size="l" component="a" href="https://vk.me/lildiarybot">Перейти к боту</Button>}
        >
          Произошла критическая ошибка, lilDiary не может отобразить эту страницу <span role='img' aria-labelledby="">😥</span>
        </Placeholder>
      </div>
    );
    const regionError = (
      <div>
        <Placeholder
          icon={<Icon56ErrorOutline />}
          title="Вы не выбрали регион"
          action={<Button size="l" component="a" href="https://vk.me/lildiarybot">Выбрать регион</Button>}
        >
          Вы не выбрали регион
        </Placeholder>
      </div>
    );
    const userError = (
      <div>
        <Placeholder
          icon={<Icon56ErrorOutline />}
          title="Вы не написали боту"
          action={<Button size="l" component="a" href="https://vk.me/lildiarybot">Написать боту</Button>}
        >
          Вы не начали диалог с ботом
        </Placeholder>
      </div>
    );
    return (
      <div>
        <PanelHeader>lilDiary | Электронный дневник</PanelHeader>
        {criticalError ? error : null}
        {chooseRegion ? regionError : null}
        {!exists ? userError : null}
        {snackbar}
      </div>
    );
  }
}

export default Main;
