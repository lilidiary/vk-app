import React from 'react';
import ReactDOM from 'react-dom';
import connect from '@vkontakte/vk-connect';
import App from './containers/App';

connect.send('VKWebAppInit', {});
connect.subscribe((e) => {
  if (e.detail.type === 'VKWebAppUpdateConfig') {
    document.body.setAttribute('scheme', e.detail.data.scheme);
    return;
  }
})

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
