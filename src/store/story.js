import axios from 'axios';
import connect from '@vkontakte/vk-connect';

const SERVER_API = 'https://api.lildiary.ru';

const parseQueryString = (string) => {
  string
    .slice(1)
    .split('&')
    .map(queryParam => {
      const kvp = queryParam.split('=');
      return { key: kvp[0], value: kvp[1] };
    })
    .reduce((query, kvp) => {
      query[kvp.key] = kvp.value;
      return query;
    }, {});
};

const queryParams = parseQueryString(window.location.search);

connect.subscribe(e => {
  if (e.detail.type === 'VKWebAppAccessTokenReceived') {
    const { data } = e.detail;
    connect.send('VKWebAppCallAPIMethod', {
      method: 'stories.getPhotoUploadServer',
      params: {
        v: '5.102',
        access_token: data.access_token,
        add_to_news: 1,
        link_text: 'signup',
        link_url: 'https://vk.com/lildiarybot'
      }
    });
  }
  if (e.detail.type === 'VKWebAppCallAPIMethodResult') {
    axios
      .post(`${SERVER_API}/app/stories`, {
        url: e.detail.data.response.upload_url,
        vkData: queryParams
      })
      .then(res => { });
  }
});
