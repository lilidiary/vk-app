import React from 'react';
import {
  FormLayout,
  Group,
  PanelHeader,
  Button,
  Input,
  Checkbox,
  Link,
  ScreenSpinner,
  Avatar,
  HeaderButton,
  Snackbar
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import PropTypes from 'prop-types';
import axios from 'axios';
import Icon28ErrorOutline from '@vkontakte/icons/dist/28/error_outline';
import { isValidPhoneNumber } from 'react-phone-number-input';
import Icon24Back from '@vkontakte/icons/dist/24/back';

const blueBackground = {
  backgroundColor: 'var(--accent)'
};

class Gosuslugi extends React.Component {
  constructor(props) {
    super();
    this.SERVER_API = props.SERVER_API;
    this.vkData = props.vkData;
    this.callback = props.callback;
    this.state = {
      checked: false,
      login: '+7',
      password: '',
      snackbar: null,
      loginValid: false,
      doing: 'pass',
      code: '',
      question: '',
      answer: '',
      time: 300
    };
    this.checkStatus();
  }

  checkBox(event) {
    this.setState({ checked: event.target.checked });
  }

  handleLogin(event) {
    const login = event.target.value;
    this.setState({ login, loginValid: isValidPhoneNumber(login) });
  }

  handlePassword(event) {
    this.setState({ password: event.target.value });
  }

  handleCode(event) {
    this.setState({ code: event.target.value });
  }

  handleQuestion(event) {
    this.setState({ answer: event.target.value });
  }

  submitCode() {
    this.callback('state', { popout: <ScreenSpinner /> });
    axios
      .post(`${this.SERVER_API}/api/gosuslugi/login`, { code: this.state.code, vkData: this.vkData })
      .then((res) => {
        if (!res.data.success) {
          this.callback('state', { popout: null });
          this.openSnackbar('Ошибка');
        }
      }).catch(() => {
        this.openSnackbar('Произошла ошибка во время авторизации');
      });
  }

  submitQuestion() {
    this.callback('state', { popout: <ScreenSpinner /> });
    axios
      .post(`${this.SERVER_API}/api/gosuslugi/login`, { answer: this.state.answer, vkData: this.vkData })
      .then((res) => {
        if (!res.data.success) {
          this.callback('state', { popout: null });
          this.openSnackbar('Ошибка');
        }
      }).catch(() => {
        this.openSnackbar('Произошла ошибка во время авторизации');
      });
  }

  openSnackbar(text) {
    this.setState({
      snackbar:
        <Snackbar// eslint-disable-line
          onClose={() => this.setState({ snackbar: null })}
          before={<Avatar size={24} style={blueBackground}><Icon28ErrorOutline fill="#fff" width={14} height={14} /></Avatar>}
        > {text}
        </Snackbar>
    });
  }

  checkStatus() {
    axios
      .post(`${this.SERVER_API}/api/gosuslugi/status`, { vkData: this.vkData })
      .then(({ data }) => {
        if (!data.proccessing) {
          this.callback('state', { popout: null });
          if (data.logged) {
            this.callback('panel', 'logged');
          } else {
            if (data.error) {
              this.openSnackbar(data.error);
              this.setState({ doing: 'pass' });
            }
          }
        } else {
          if (data.state === 'tfa') {
            this.setState({ doing: data.state, time: data.time });
          }
          if (data.state === 'question') {
            this.setState({ doing: data.state, question: data.question });
          }
          if (['proccessing', 'waiting'].includes(data.state)) {
            this.callback('state', { popout: <ScreenSpinner /> });
          } else {
            this.callback('state', { popout: null });
          }
        }
      }).catch(() => {
        this.openSnackbar('Произошла ошибка');
        this.callback('state', { popout: null });
      });
    setTimeout(() => this.checkStatus(), 1000);
  }

  submit() {
    this.callback('state', { popout: <ScreenSpinner /> });
    axios
      .post(`${this.SERVER_API}/api/gosuslugi/login`, { login: this.state.login, password: this.state.password, vkData: this.vkData })
      .then((res) => {
        if (!res.data.success) {
          this.callback('state', { popout: null });
          if (res.data.reason === 'password') {
            this.openSnackbar('Неверный пароль');
          } else if (res.data.reason === 'limit') {
            this.openSnackbar('Вы исчерпали лимит попыток, приходите через час');
          } else {
            this.openSnackbar('Проблемы на сайте электронного дневника');
          }
        }
      }).catch(() => {
        this.openSnackbar('Произошла ошибка во время авторизации');
      });
  }

  render() {
    const pass = (
      <Group title="Введите данные госуслуг" description="Будьте внимательны - количество попыток входа ограниченно">
        <FormLayout>
          <Input id='login' top='Телефон' status={this.state.loginValid ? 'valid' : 'error'} value={this.state.login} onChange={(e) => this.handleLogin(e)} type="text" />
          <Input id='password' top='Пароль' onChange={(e) => this.handlePassword(e)} type="password" />
          <Checkbox onChange={(e) => this.checkBox(e)}>Я согласен с <Link href="https://lildiary.ru/privacy.pdf">политикой приватности</Link></Checkbox>
          <Button onClick={(e) => this.submit(e)} disabled={!this.state.checked || !this.state.loginValid || this.state.password.length < 3} size="xl">Войти</Button>
        </FormLayout>
      </Group>
    );
    const tfa = (
      <Group title="Введите код ">
        <FormLayout>
          <Input id='tfa' bottom={`${this.state.time} секунд`} top='Код входа' onChange={(e) => this.handleCode(e)} type="text" />
          <Button onClick={(e) => this.submitCode(e)} disabled={isNaN(this.state.code) || parseInt(this.state.code, 10) + ''.length < 6} size="xl">Войти</Button>
        </FormLayout>
      </Group>
    );

    const question = (
      <Group title="Ответьте на вопрос">
        <FormLayout>
          <Input id='tfa' top={this.state.question} onChange={(e) => this.handleQuestion(e)} type="text" />
          <Button onClick={(e) => this.submitQuestion(e)} size="xl">Войти</Button>
        </FormLayout>
      </Group>
    );

    return (
      <div>
        <PanelHeader left={<HeaderButton onClick={() => { this.callback('panel', 'login'); }}><Icon24Back /></HeaderButton>}>Госуслуги</PanelHeader>
        {this.state.doing === 'pass' ? pass : null}
        {this.state.doing === 'tfa' ? tfa : null}
        {this.state.doing === 'question' ? question : null}
        {this.state.snackbar}
      </div>
    );
  }
}

Gosuslugi.propTypes = {
  SERVER_API: PropTypes.string.isRequired,
  vkData: PropTypes.any.isRequired,
  callback: PropTypes.any.isRequired
};
export default Gosuslugi;
