import React from 'react';
import {
  Panel,
  View,
  ScreenSpinner,
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import Main from './main';
import Login from './login';
import Gosuslugi from './gosuslugi';
import Logged from './logged';
import Admin from './admin';


let SERVER_API = 'https://login.lildiary.ru';
const currentURL = new URL(document.location);
if (currentURL.host === 'localhost') {
  SERVER_API = 'http://localhost:8081';
}

function parseQueryString(string) {
  return string
    .slice(1)
    .split('&')
    .map(queryParam => {
      const kvp = queryParam.split('=');
      return { key: kvp[0], value: kvp[1] };
    })
    .reduce((query, kvp) => {
      query[kvp.key] = kvp.value;
      return query;
    }, {});
}

const vkData = parseQueryString(window.location.search);


class App extends React.Component {
  constructor() {
    super();
    this.state = {
      activePanel: 'main',
      popout: <ScreenSpinner />
    };

    this.callback = this.callback.bind(this);
  }

  callback(type, args) {
    if (type === 'state') {
      this.setState(args);
    }
    if (type === 'panel') {
      this.setState({ activePanel: args });
    }
  }

  render() {
    return (
      <View activePanel={this.state.activePanel} popout={this.state.popout}>
        <Panel id='main'>
          <Main SERVER_API={SERVER_API} callback={this.callback} vkData={vkData} />
        </Panel>
        <Panel id='login'>
          <Login SERVER_API={SERVER_API} vkData={vkData} callback={this.callback} />
        </Panel>
        <Panel id='gosuslugi'>
          <Gosuslugi SERVER_API={SERVER_API} vkData={vkData} callback={this.callback} />
        </Panel>
        <Panel id='logged'>
          <Logged />
        </Panel>
        <Panel id='admin'>
          <Admin SERVER_API={SERVER_API} vkData={vkData} callback={this.callback} />
        </Panel>
      </View>
    );
  }
}

export default App;
