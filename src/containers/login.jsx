import React from 'react';
import {
  FormLayout,
  Group,
  PanelHeader,
  Button,
  Div,
  Input,
  Checkbox,
  Link,
  ScreenSpinner,
  Avatar,
  Snackbar,
  Tooltip,
  Cell,
  List
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import connect from '@vkontakte/vk-connect';
import PropTypes from 'prop-types';
import axios from 'axios';
import Icon28ErrorOutline from '@vkontakte/icons/dist/28/error_outline';

const blueBackground = {
  backgroundColor: 'var(--accent)'
};

class Login extends React.Component {
  constructor(props) {
    super();
    this.SERVER_API = props.SERVER_API;
    this.vkData = props.vkData;
    this.callback = props.callback;
    this.state = {
      checked: false,
      login: '',
      password: '',
      snackbar: null,
      tooltip: !localStorage.getItem('tooltip'),
      mutual: []
    };
  }

  componentDidMount() {
    connect.sendPromise('VKWebAppGetAuthToken', { app_id: 7183418, scope: 'groups' })
      // eslint-disable-next-line
      .then(({ access_token }) => {
        connect.sendPromise('VKWebAppCallAPIMethod',
          { method: 'groups.getMembers', params: { access_token, group_id: 'lildiarybot', fields: 'photo_200', filter: 'friends', v: '5.103' } })
          .then(({ response }) => {
            this.setState({ mutual: response.items });
          });
      });
  }


  handleLogin(event) {
    this.setState({ login: event.target.value });
  }

  handlePassword(event) {
    this.setState({ password: event.target.value });
  }

  openSnackbar(text) {
    this.setState({
      snackbar:
        <Snackbar// eslint-disable-line
          onClose={() => this.setState({ snackbar: null })}
          before={<Avatar size={24} style={blueBackground}><Icon28ErrorOutline fill='#fff' width={14} height={14} /></Avatar>}
        > {text}
        </Snackbar>
    });
  }


  submit() {
    this.callback('state', { popout: <ScreenSpinner /> });
    axios
      .post(`${this.SERVER_API}/api/login`, { login: this.state.login, password: this.state.password, vkData: this.vkData })
      .then((res) => {
        this.callback('state', { popout: null });

        if (res.data.success) {
          this.openSnackbar('Вы успешно вошли');
        } else {
          if (res.data.reason === 'password') {
            this.openSnackbar('Неверный пароль');
          } else if (res.data.reason === 'limit') {
            this.openSnackbar('Вы исчерпали лимит попыток, мы добавим вам папытку через час');
          } else {
            this.openSnackbar('Проблемы на сайте электронного дневника');
          }
        }
      }).catch(() => {
        this.callback('state', { popout: null });
        this.openSnackbar('Произошла ошибка во время авторизации');
      });
  }

  render() {
    const { mutual } = this.state;
    const mutuals = mutual.map((item) =>
      (
        <Cell before={<Avatar src={item.photo_200} />}>{item.first_name} {item.last_name}</Cell>
      ));
    let mutualGroup = null;
    if (mutual.length > 0) {
      mutualGroup = (
        <Group title='Ваши друзья, которые знают о lilDiary'>
          <List>
            {mutuals}
          </List>
        </Group>
      );
    }
    return (
      <div>
        <PanelHeader>lilDiary | Авторизация</PanelHeader>
        <Group title='Вход по паролю' description='Пользователи выбирают lilDiary за простоту, с которой можно следить за оценками в школе'>
          <FormLayout>
            <Input id='login' top='Логин' value={this.state.login} onChange={(e) => this.handleLogin(e)} type='text' />
            <Input id='password' top='Пароль' onChange={(e) => this.handlePassword(e)} type='password' />
            <Checkbox onChange={(e) => this.setState({ checked: e.target.checked })}>Я согласен с <Link href='https://lildiary.ru/privacy.pdf'>политикой приватности</Link></Checkbox>
            <Button onClick={(e) => this.submit(e)} disabled={!this.state.checked || this.state.login.length < 3 || this.state.password.length < 3} size='xl'>Войти</Button>
          </FormLayout>
        </Group>
        <Group title='Вход через Госуслуги'>
          <Div style={{ display: 'flex' }}>
            <Tooltip
              isShown={this.state.tooltip}
              onClose={() => {
                this.setState({ tooltip: false });
                localStorage.setItem('tooltip', true);
              }}
              text='Мы тут добавили возможность входа через госуслуги'
            >
              <Button component='a' href='#' stretched size='xl' onClick={() => this.callback('panel', 'gosuslugi')} level='secondary'>
                Госуслуги
              </Button>
            </Tooltip>
          </Div>
        </Group>
        {mutualGroup}
        {this.state.snackbar}
      </div>
    );
  }
}

Login.propTypes = {
  SERVER_API: PropTypes.string.isRequired,
  vkData: PropTypes.any.isRequired,
  callback: PropTypes.any.isRequired
};
export default Login;
