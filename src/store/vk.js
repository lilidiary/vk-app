import Immutable from 'seamless-immutable';

const initialState = Immutable({
  vk: undefined,
});

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    default:
      return state;
  }
}

export function getAccessToken(state) {
  return state.vk.accessToken;
}

export function getNotificationStatus(state) {
  return state.vk.notificationStatus;
}

export function getInsets(state) {
  return state.vk.insets;
}

export function getLogs(state) {
  return JSON.stringify(state.vk.logs);
}
