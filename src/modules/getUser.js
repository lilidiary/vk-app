import axios from 'axios';

export function getUser(url, userID, vkData) {
  return axios
    .post(`${url}/api/getUser`, { vkData, userID });
}