import React from 'react';
import {
  PanelHeader,
  Button,
  Placeholder,
} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import Icon56CheckCircleOutline from '@vkontakte/icons/dist/56/check_circle_outline';

class Logged extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div>
        <PanelHeader>lilDiary | Успешная авторизация</PanelHeader>
        <Placeholder
          icon={<Icon56CheckCircleOutline />}
          title="Успешная авторизация"
          action={<Button size="l" component="a" href="https://vk.me/lildiarybot">Продолжить</Button>}
        >
          Мы вас так долго ждали. Ура! Пользуйтесь вашим дневником lilDiary
        </Placeholder>
      </div>
    );
  }
}

export default Logged;
