
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import vk from './vk';

export default function configureStore(initialState = {}) {
  const createStoreWithMiddleware = applyMiddleware(
    thunk,
    createLogger()
  )(createStore);

  const store = createStoreWithMiddleware(
    combineReducers({
      vk
    }),
    initialState
  );

  window.store = store;
  return store;
}
