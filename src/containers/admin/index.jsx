import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import Dashboard from './admin';

export default function App(props) {
  return (
    <Router>
      <Switch>
        <Route path='/'>
          <Dashboard {...props} />
        </Route>
      </Switch>
    </Router>
  );
}
